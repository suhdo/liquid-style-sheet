<br />
<div class="tiles">
	<div class="size-70">
		<div class="padding" template="contact_form">
		<h1>Contato</h1>
		<h2 class="color-gray">Como nosso negócio é a comunicação, aguardamos ansiosos um contato seu. Seja pra dar "oi", sugestões, criticar ou trocar ideias. Tudo é válido, fale aí com a gente.</h2>
		<br />
		<form action="spam.html" method="post" id="myform" class="pt14"/>
			<label>Nome:*</label>
			<br />
			<input type="text" name="Nome" value="" class="rounded shaded padding5" size="65" />
			<br />
			<br />
			<label>E-mail:*</label>
			<br />
			<input type="text" name="Email" value="" class="rounded shaded padding5" size="65" />
			<br />
			<br />
			<label>Site:</label>
			<br />
			<input type="text" name="Site" value="" class="rounded shaded padding5" size="65" />
			<br />
			<br />
			<label>Assunto:*</label>
			<br />
			<input type="text" name="Assunto" value="" class="rounded shaded padding5" size="65" />
			<br />
			<br />
			<label>Mensagem:*</label>
			<br />
			<textarea name="Mensagem" value="" class="rounded shaded padding5" style="width:580px; height:170px;"></textarea>
			<div class="break"></div>
			<br />
			<fieldset class="block">
				<span class="inline legend">* Campo Obrigatório</span>
				<p class="align-right">
				<a href="javascript:void(0);" class="inline b rounded shaded padding5 pages default-bt " style="margin-right:100px;" id="postform" onclick="return postform();">Enviar</a>
				<p>
			</fieldset>
		</form>
		<br />
		<br />
		<br />
		</div>
	</div>
	<div class="size-30">
		<div class="padding">
			<form action="resultados-da-busca.html" method="post" name="searchForm">
				<input type="text" name="search" class="rounded shaded padding5" style="width:220px;"/>
				<a class="hover margin-left float-right" onclick="document.searchForm.submit();"><img src="layout/img/search.jpg" /></a>
			</form>
			<br />
			<br />
			<h1 class="pt22">Siga a HiP!</h1>
			<br />
			<a href="http://www.facebook.com/hipcomunicacao" target="_blank"><img src="layout/img/facebook.jpg" width="28"/></a>
			<a href="https://twitter.com/agenciahip" target="_blank"><img src="layout/img/twitter.jpg" width="28"/></a>
			<a href="http://www.youtube.com/user/agenciahip" target="_blank"><img src="layout/img/youtube.jpg" width="28"/></a>
			<a href="https://soundcloud.com/hipcomunicacao" target="_blank"><img src="layout/img/cloud.jpg" width="28"/></a>
			<br />
			<br />
			<p>
				<a href="http://www.hipweb.ppg.br/sis/index.aspx" target="_blank" class="inline rounded shaded padding block align-center font-medium b pages default-bt ">Acesso Cliente</a>
				<a href="https://www.yousendit.com/transfer.php?action=dropbox&dropbox=HiP" target="_blank" class="margin-top inline rounded shaded padding block align-center font-medium b pages default-bt ">Enviar Arquivos</a>
			</p>

		</div>
	</div>
	<div class="break"></div>	

</div>
