
<br />
<div class="tiles">
	<div class="size-70">
		<div class="padding" template="main_menu">
		<h1>Equipe</h1>
		<br />
		</div>
	</div>
	<div class="size-30">
		<div class="padding" template="main_products">
			<form action="resultados-da-busca.html" method="post" name="searchForm">
				<input type="text" name="search" class="rounded shaded padding5" style="width:220px;"/>
				<a class="hover margin-left float-right" onclick="document.searchForm.submit();"><img src="layout/img/search.jpg" /></a>
			</form>
			<br />
			<br />
			<h1 class="pt22">Siga a HiP!</h1>
			<br />
			<a href="http://www.facebook.com/hipcomunicacao" target="_blank"><img src="layout/img/facebook.jpg" width="28"/></a>
			<a href="https://twitter.com/agenciahip" target="_blank"><img src="layout/img/twitter.jpg" width="28"/></a>
			<a href="http://www.youtube.com/user/agenciahip" target="_blank"><img src="layout/img/youtube.jpg" width="28"/></a>
			<a href="https://soundcloud.com/hipcomunicacao" target="_blank"><img src="layout/img/cloud.jpg" width="28"/></a>
			<br />
			<br />
			<p>
				<a href="trabalhe-conosco.html" class="inline rounded shaded padding block align-center font-medium b pages default-bt ">Trabalhe conosco</a>
			</p>

		</div>
	</div>
	<div class="break"></div>	

</div>
