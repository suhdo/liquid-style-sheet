<div class="bg-white ">
	<div class="tiles-container">
		<div class="tiles">
			<div class="size-100">
				<div class="padding align-left metro green">
				<h1 class="r text-">Liquid Style Sheet</h1>
				</div>
			</div>
			<div class="size-20">
				<div class="rounded margin padding">
				<h1 class="r">Examples</h1>
				<br />
				<ul class="pt14">
					<li><a>Fluid Design</a></li>
				</ul>
				</div>
			</div>
			<div class="size-80 style1">
				<div class="rounded">
					<div class="tiles-container">
						<div class="tiles">
							<div class="padding margin" anchor="fluid-design">
								<h1 class="r">Fluid Design</h1>
								<div class="break"></div>
								<br />

								<div class="size-100 shaded white inline">
									<p class="padding margin shaded">100%</p>
								</div>

								<div class="size-50 shaded white inline">
									<p class="padding margin shaded">50%</p>
								</div>
								<div class="size-50 shaded white inline">
									<p class="padding margin shaded">50%</p>
								</div>

								<div class="size-25 shaded white inline">
									<p class="padding margin shaded">25%</p>
								</div>
								<div class="size-25 shaded white inline">
									<p class="padding margin shaded">25%</p>
								</div>
								<div class="size-25 shaded white inline">
									<p class="padding margin shaded">25%</p>
								</div>
								<div class="size-25 shaded white inline">
									<p class="padding margin shaded">25%</p>
								</div>

								<div class="size-75 shaded white inline">
									<p class="padding margin shaded">75%</p>
								</div>
								<div class="size-25 shaded white inline">
									<p class="padding margin shaded">25%</p>
								</div>

								<div class="break"></div>
								<br />
								<div class="size-25 shaded white">25%</div>
								<div class="size-25 shaded white">25%</div>
								<div class="size-25 shaded white">25%</div>
								<div class="size-25 shaded white">25%</div>
								<div class="break"></div>
								<br />
								<div class="size-20 shaded white">20%</div>
								<div class="size-20 shaded white">20%</div>
								<div class="size-20 shaded white">20%</div>
								<div class="size-20 shaded white">20%</div>
								<div class="size-20 shaded white">20%</div>
								<div class="break"></div>
								<br />
								<div class="size-10 shaded white">10%</div>
								<div class="size-10 shaded white">10%</div>
								<div class="size-10 shaded white">10%</div>
								<div class="size-10 shaded white">10%</div>
								<div class="size-10 shaded white">10%</div>
								<div class="size-10 shaded white">10%</div>
								<div class="size-10 shaded white">10%</div>
								<div class="size-10 shaded white">10%</div>
								<div class="size-10 shaded white">10%</div>
								<div class="size-10 shaded white">10%</div>
								<div class="break"></div>
								<br />
							</div>

							<hr />

							<div class="padding margin" anchor="buttons">
								<h1 class="r">Buttons</h1>
								<br />
								<p class="marginy5">Default Buttons</p>
								<button class="button padding5 rounded inline">Default</button>
								<button class="button caution padding5 rounded">Caution</button>
								<button class="button danger padding5 rounded">Danger</button>
								<button class="button okay padding5 rounded">Danger</button>
								<button class="button active padding5 rounded">Danger</button>
								<br />
								<br />

								<p class="marginy5">Customization</p>
								<button class="button okay padding5 left-rounded">On</button><button class="button padding5 right-rounded b">Off</button>
								<span class="inline">&nbsp;</span>
								<button class="button active padding5 rounded text-shadow">&lang;&lang;</button>
								<button class="button active padding5 rounded b text-shadow">1</button>
								<button class="button active padding5 rounded text-shadow">2</button>
								<button class="button active padding5 rounded text-shadow">&rang;&rang;</button>
								<br />
								<br />
								<button class="button padding rounded">Custom</button>
								<button class="button padding5 rounded pt18">Custom</button>
								<button class="button padding5 pt14">Custom</button>
								<button class="button padding5 rounded">Custom</button>
								<button class="button padding5 rounded legend">Custom</button>

							</div>


							<hr />

							<div anchor="buttons">
								<div class="tiles-container">
									<div class="tiles">
										<div class="padding margin" anchor="fluid-design">
											<h1 class="r">Metro Style</h1>
											<br />

											<div class="size-25">
												<div class="margin2 padding metro green shadowed">
													<h2>Silver</h2>
													<p>Metro</p>
													<p>Fluid 25%</p>
												</div>
											</div>	

											<div class="size-25">
												<div class="margin2 padding metro orange shadowed">
													<h2>Silver</h2>
													<p>Metro</p>
													<p>Fluid 25%</p>
												</div>
											</div>	

											<div class="size-50">
												<div class="margin2 padding metro blue shadowed">
													<h2>Silver</h2>
													<p>Metro</p>
													<p>Fluid 50%</p>
												</div>
											</div>	
											<div class="size-50">
												<div class="margin2 padding metro pink shadowed">
													<h2>Silver</h2>
													<p>Metro</p>
													<p>Fluid 50%</p>
												</div>
											</div>	
											<div class="size-50">
												<div class="margin2 padding metro purple shadowed">
													<h2>Silver</h2>
													<p>Metro</p>
													<p>Fluid 50%</p>
												</div>
											</div>	
											<div class="size-75">
												<div class="margin2 padding metro gray shadowed">
													<h2>Silver</h2>
													<p>Metro</p>
													<p>Fluid 75%</p>
												</div>
											</div>	
											<div class="size-25">
												<div class="margin2 padding metro silver shadowed">
													<h2>Silver</h2>
													<p>Metro</p>
													<p>Fluid 25%</p>
												</div>
											</div>	
											<div class="break"></div>
											<br />
										</div>
									</div>
								</div>
							</div>






							<hr />

							<div anchor="buttons">
								<div class="tiles-container">
									<div class="tiles">
										<div class="padding margin" anchor="fluid-design">
											<h1 class="r">Text Formating</h1>
											<br />

											<div class="size-50">
												<div class="padding justify">
													<h1>Jusitify</h1>
													<h2 class="marginy5">Justify as text paragraph</h2>												
													<p>Mauris sed dolor a purus dictum pulvinar. Aliquam ut lectus augue, ac fringilla lacus. Pellentesque commodo nisl a orci luctus sed luctus sapien lacinia. Sed ut velit et felis placerat tempor.</p>
												</div>
											</div>	


											<div class="size-50">
												<div class="padding align-left">
													<h1>Align Left</h1>
													<h2 class="marginy5">Align text in left</h2>												
													<p>Mauris sed dolor a purus dictum pulvinar. Aliquam ut lectus augue, ac fringilla lacus. Pellentesque commodo nisl a orci luctus sed luctus sapien lacinia. Sed ut velit et felis placerat tempor.</p>
												</div>
											</div>	

											<div class="size-50">
												<div class="padding align-right">
													<h1>Align Right</h1>
													<h2 class="marginy5">Align text in right</h2>												
													<p>Mauris sed dolor a purus dictum pulvinar. Aliquam ut lectus augue, ac fringilla lacus. Pellentesque commodo nisl a orci luctus sed luctus sapien lacinia. Sed ut velit et felis placerat tempor.</p>
												</div>
											</div>	

											<div class="size-50">
												<div class="padding align-center">
													<h1>Align Center</h1>
													<h2 class="marginy5">Align text in center</h2>												
													<p>Mauris sed dolor a purus dictum pulvinar. Aliquam ut lectus augue, ac fringilla lacus. Pellentesque commodo nisl a orci luctus sed luctus sapien lacinia. Sed ut velit et felis placerat tempor.</p>
												</div>
											</div>	

											<div class="break"></div>
											<br />
										</div>
									</div>
								</div>
							</div>






							<hr />

							<div anchor="buttons">
								<div class="tiles-container">
									<div class="tiles">
										<div class="padding margin" anchor="fluid-design">
											<h1 class="r">Effects</h1>
											<h2 class="b margin-top">Drop Shadow</h2>

													
											<div class="dark-10 marginy5">
												<div class="size-20">
													<div class="padding margin metro silver shaded">
														<h2 class="b">Shaded</h2>
														<p class="marginy5">Make an easy drop shadow on element</p>												
													</div>
												</div>	

												<div class="size-20">
													<div class="padding margin metro silver shadowed">
														<h2 class="b">Shadowed</h2>
														<p class="marginy5">Make an easy drop offset shadow on element</p>												
													</div>
												</div>	

												<div class="size-20">
													<div class="padding margin metro silver reverse-shadow">
														<h2 class="b">Reverse Shadow</h2>
														<p class="marginy5">Make an easy reverse shadow on element</p>												
													</div>
												</div>	

												<div class="size-20">
													<div class="padding margin metro silver inset-shadow">
														<h2 class="b">Inset Shadow</h2>
														<p class="marginy5">Make an easy drop inset shadow on element</p>												
													</div>
												</div>	

												<div class="break"></div>
											</div>	

											<br />
											<h2 class="b">Corner Rounded</h2>

											<div class="dark-10 marginy5">
												<div class="size-20">
													<div class="padding margin metro gray rounded">
														<h2 class="b">Rounded</h2>
														<p class="marginy5">Make an easy rounded corner element</p>												
													</div>
												</div>	

												<div class="size-20">
													<div class="padding margin metro gray top-rounded">
														<h2 class="b">Top Rounded</h2>
														<p class="marginy5">Make an easy element rounded on top</p>												
													</div>
												</div>	

												<div class="size-20">
													<div class="padding margin metro gray bottom-rounded">
														<h2 class="b">Bottom Rounded</h2>
														<p class="marginy5">Make an easy element rounded on bottom </p>												
													</div>
												</div>	

												<div class="size-20">
													<div class="padding margin metro gray left-rounded">
														<h2 class="b">Left Rounded</h2>
														<p class="marginy5">Make an easy element rounded on left</p>												
													</div>
												</div>	

												<div class="size-20">
													<div class="padding margin metro gray right-rounded">
														<h2 class="b">Right Rounded</h2>
														<p class="marginy5">Make an easy element rounded on right </p>												
													</div>
												</div>	

												<div class="break"></div>
											</div>


											<br />
											<h2 class="b">Blur</h2>

											<div class="dark-10 marginy5">

												<div class="size-20">
													<div class="padding margin metro gray blur-soft">
														<h2 class="b">Soft Blur</h2>
														<p class="marginy5">Make a soft blur effect on element</p>												
													</div>
													<div class="margin">
														<h2 class="b">Soft Blur</h2>
														<p class="marginy5">Make a soft blur effect on element</p>												
													</div>
												</div>	


												<div class="size-20">
													<div class="padding margin metro gray blur">
														<h2 class="b">Blur</h2>
														<p class="marginy5">Make a blur effect on element</p>												
													</div>
													<div class="margin">
														<h2 class="b">Blur</h2>
														<p class="marginy5">Make a blur effect on element</p>												
													</div>
												</div>	

												<div class="size-20">
													<div class="padding margin metro gray blur-hard">
														<h2 class="b">Hard Blur</h2>
														<p class="marginy5">Make blur effect on element</p>												
													</div>
													<div class="margin">
														<h2 class="b">Hard Blur</h2>
														<p class="marginy5">Make an hard blur effect on element</p>												
													</div>
												</div>	


												<div class="break"></div>
											</div>



											<br />
											<h2 class="b">Rotation</h2>

											<div class="dark-10 marginy5">

												<div class="size-20">
													<div class="padding margin">
														<h2 class="inline deg45 metro silver shaded padding">45&deg;</h2>
													</div>
													<div class="margin">
														<h2 class="">Rotate 45&deg;</h2>
													</div>
												</div>	
												<div class="size-20">
													<div class="padding margin">
														<h2 class="inline deg90 metro silver shaded padding">90&deg;</h2>
													</div>
													<div class="margin">
														<h2 class="">Rotate 90&deg;</h2>
													</div>
												</div>	
												<div class="size-20">
													<div class="padding margin">
														<h2 class="inline deg180 metro silver shaded padding">180&deg;</h2>
													</div>
													<div class="margin">
														<h2 class="">Rotate 180&deg;</h2>
													</div>
												</div>	




												<div class="break"></div>
											</div>


										</div>
									</div>
								</div>
							</div>









							<hr />

							<div anchor="buttons">
								<div class="tiles-container">
									<div class="tiles">
										<div class="padding margin" anchor="fluid-design">
											<h1 class="r">Text Effects</h1>
											<h2 class="b margin-top">Filters</h2>
													
											<div class="dark-10 marginy5">
												<div class="size-33">
													<div class="padding margin metro silver shaded">
														<h2 class="b text-glow font-giant color-orange">Normal</h2>
														<p class="marginy5">Normal text style</p>												
													</div>
												</div>	

												<div class="size-33">
													<div class="padding margin metro silver shaded">
														<h2 class="text-glow b font-giant color-orange">Text Glow</h2>
														<p class="marginy5">Apply effect: Text Glow</p>												
													</div>
												</div>	


												<div class="size-33">
													<div class="padding margin metro silver shaded">
														<h2 class="text-carved b font-giant color-orange">Text Carved</h2>
														<p class="marginy5">Apply effect: Text Carved</p>												
													</div>
												</div>	

												<div class="size-33">
													<div class="padding margin metro silver shaded">
														<h2 class="text-shadow b font-giant color-orange">Text Shadow</h2>
														<p class="marginy5">Apply effect: Text Shadow</p>												
													</div>
												</div>	

												<div class="size-33">
													<div class="padding margin metro silver shaded">
														<h2 class="text-shaded b font-giant color-orange">Text Shaded</h2>
														<p class="marginy5">Apply effect: Text Shaded</p>												
													</div>
												</div>	





												<div class="break"></div>
											</div>	


										</div>
									</div>
								</div>
							</div>



						</div>
					</div>
				</div>
			</div>
			<div class="size-100">
				<div class="padding align-left">

				</div>
			</div>
			<div class="break"></div>
		</div>
	</div>
</div>


