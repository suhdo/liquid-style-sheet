<br />
<div class="tiles">
	<div class="size-70">
		<div class="padding" template="main_news">
			<?php
			$banners = new \obj\template;
			$banners->homepageNews("read-full",1);
			?>
			<br />
			<br />
		</div>
	</div>	
	<div class="size-30">
		<div class="padding">
			<input type="text" name="search" class="rounded shaded padding5" style="width:220px;"/>
			<a class="hover margin-left float-right"><img src="layout/img/search.jpg" /></a>
			<br />
			<br />
			<h1 class="pt22">Siga a HiP!</h1>
			<br />
			<a href="http://www.facebook.com/hipcomunicacao" target="_blank"><img src="layout/img/facebook.jpg" width="28"/></a>
			<a href="https://twitter.com/agenciahip" target="_blank"><img src="layout/img/twitter.jpg" width="28"/></a>
			<a href="http://www.youtube.com/user/agenciahip" target="_blank"><img src="layout/img/youtube.jpg" width="28"/></a>
			<a href="https://soundcloud.com/hipcomunicacao" target="_blank"><img src="layout/img/cloud.jpg" width="28"/></a>
		</div>
	</div>
	<div class="break"></div>	
</div>
