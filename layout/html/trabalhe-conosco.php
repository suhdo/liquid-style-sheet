<br />
<div class="tiles">
	<div class="size-70">
		<div class="padding" template="main_menu">
		<h1>Trabalhe Conosco</h1>
		<br />
		<form action="spam.php" enctype="multipart/form-data" method="post" class="tiles" name="myform" id="myform">
			<div class="size-50 pt14">
				<div class="padding">
					<label>Nome:*</label>
					<br />
					<input type="text" name="Nome" value="" class="rounded shaded padding5" size="35" />
					<br />
					<br />
					<label>Sexo:*</label>
					<br />
					<select name="Sexo" class="rounded shaded padding5" style="width:300px;">
						<option value=""></option>
						<option value="Feminino">Feminino</option>
						<option value="Masculino">Masculino</option>
					</select>
					<br />
					<br />
					<label>Telefone:*</label>
					<br />
					<input type="text" name="Telefone" id="fone" value="" class="rounded shaded padding5" size="35" />
					<br />
					<br />
					<label>Cidade:*</label>
					<br />
					<input type="text" name="Cidade" value="" class="rounded shaded padding5" size="35" />
					<br />
					<br />
					<label>Estado:*</label>
					<br />
					<select name="Estado" class="rounded shaded padding5" style="width:300px;">
						<option value=""></option>
						<option value="AC">Acre</option>
						<option value="AL">Alagoas</option>
						<option value="AM">Amazonas</option>
						<option value="AP">Amapá</option>
						<option value="BA">Bahia</option>
						<option value="CE">Ceará</option>
						<option value="DF">Distrito Federal</option>
						<option value="ES">Espirito Santo</option>
						<option value="GO">Goiás</option>
						<option value="MA">Maranhão</option>
						<option value="MG">Minas Gerais</option>
						<option value="MS">Mato Grosso do Sul</option>
						<option value="MT">Mato Grosso</option>
						<option value="PA">Pará</option>
						<option value="PB">Paraíba</option>
						<option value="PE">Pernambuco</option>
						<option value="PI">Piauí</option>
						<option value="PR">Paraná</option>
						<option value="RJ">Rio de Janeiro</option>
						<option value="RN">Rio Grande do Norte</option>
						<option value="RO">Rondônia</option>
						<option value="RR">Roraima</option>
						<option value="RS">Rio Grande do Sul</option>
						<option value="SC">Santa Catarina</option>
						<option value="SE">Sergipe</option>
						<option value="SP">São Paulo</option>
						<option value="TO">Tocantins</option>
					</select>
					<br />
					<br />
					<p class="legend">* Campo obrigatório</p>
				</div>
			</div>


			<div class="size-50 pt14">
				<div class="padding">
					<label>Área Pretendida:*</label>
					<br />
					<select name="Cargo" class="rounded shaded padding5" style="width:300px;">
							<option value=""></option>
						<option value="Administrativo">Administrativo</option>
						<option value="Atendimento">Atendimento</option>
						<option value="Criação">Criação</option>
						<option value="Mídia">Mídia</option>
						<option value="Mídias Sociais">Mídias Sociais</option>
						<option value="Orçamento">Orçamento</option>
						<option value="Redação">Redação</option>
						<option value="Tráfego">Tráfego</option>
					</select>
					<br />
					<br />
					<label>Data de Nascimento:*</label>
					<br />
					<input type="text" name="Nascimento" id="data" value="" class="datepicker rounded shaded padding5" size="35" />
					<br />
					<br />
					<label>E-mail:*</label>
					<br />
					<input type="text" name="Email" value="" class="rounded shaded padding5" size="35" />
					<br />
					<br />
					<label>Site/Portifólio:</label>
					<br />
					<input type="text" name="Portifolio" value="" class="rounded shaded padding5" size="35" />
					<br />
					<br />
					<label>Anexe seu currículo ou portifólio:*</label>
					<br />
					<input type="file" name="Anexo" value="" class="rounded shaded padding5" size="35" onchange="valida_extensao(this);"/>
					<br />
					<br />
					<p class="align-right">
						<a href="javascript:void(0);" class="inline b rounded shaded padding5 pages default-bt " style="margin-right:20px;" onclick="return validateForm();">Enviar</a>
					</p>
				</div>
			</div>
		</form>
		</div>
	</div>
	<div class="size-30">
		<div class="padding" template="main_products">
			<form action="resultados-da-busca.html" method="post" name="searchForm">
				<input type="text" name="search" class="rounded shaded padding5" style="width:220px;"/>
				<a lass="hover margin-left float-right" onclick="document.searchForm.submit();"><img src="layout/img/search.jpg" /></a>
			</form>
			<br />
			<br />
			<h1 class="pt22">Siga a HiP!</h1>
			<br />
			<a href="http://www.facebook.com/hipcomunicacao" class=""><img src="layout/img/facebook.jpg" width="28"/></a>
			<a href="https://twitter.com/agenciahip" class=""><img src="layout/img/twitter.jpg" width="28"/></a>
			<a href="http://www.youtube.com/user/agenciahip" class=""><img src="layout/img/youtube.jpg" width="28"/></a>
			<a href="https://soundcloud.com/hipcomunicacao" class=""><img src="layout/img/cloud.jpg" width="28"/></a>
		</div>
	</div>
	<div class="break"></div>	

</div>
