<br />
<div class="tiles">
	<div class="size-70">
		<div class="padding" template="main_news">
		<h1>Resultados da busca</h1>
		<br />
		<?php

			$search = postvar('search');
			$find = new \obj\sql;
			$news = $find->seek("*","homepage.news","title ilike '%$search%' or content ilike '%$search%' or tags ilike '%$search%'","order by id asc",4,0);

			if($news->num<1) 
				echo '<p class="align-center font-big neutral-color padding-super">Nenhum resultado encontrado!</p>';
			else	
			foreach($news->rows as $row) 
				echo '<a href="ler/'.$row['url'].'" class="hover"><strong>'.$row['title'].'</strong><br/ ><span>'.limitstr($row['content'],200).'</span></a><br /><br />';

		?>
		</div>
	</div>	
	<div class="size-30">
		<div class="padding">
			<form action="resultados-da-busca.html" method="post" name="searchForm">
				<input type="text" name="search" class="rounded shaded padding5" style="width:220px;"/>
				<a class="hover margin-left float-right" onclick="document.searchForm.submit();"><img src="layout/img/search.jpg" /></a>
			</form>
			<br />
			<br />
			<h1 class="pt22">Siga a HiP!</h1>
			<br />
			<a href="http://www.facebook.com/hipcomunicacao" target="_blank"><img src="layout/img/facebook.jpg" width="28"/></a>
			<a href="https://twitter.com/agenciahip" target="_blank"><img src="layout/img/twitter.jpg" width="28"/></a>
			<a href="http://www.youtube.com/user/agenciahip" target="_blank"><img src="layout/img/youtube.jpg" width="28"/></a>
			<a href="https://soundcloud.com/hipcomunicacao" target="_blank"><img src="layout/img/cloud.jpg" width="28"/></a>

			<br />
			<br />
		
		</div>
	</div>
	<div class="break"></div>
</div>
