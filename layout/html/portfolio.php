<br />
<div class="tiles">
	<div class="size-70">
		<div class="padding" template="main_menu">
		<h1>Portifólio</h1>
		</div>
	</div>
	<div class="size-30">
		<div class="padding" template="main_products">
			<form action="portifolio.html" method="post" name="searchForm">
				<input type="text" name="search" class="rounded shaded padding5" style="width:220px;"/>
				<a class="hover margin-left float-right" onclick="document.searchForm.submit();"><img src="layout/img/search.jpg" /></a>
			</form>
		</div>
	</div>
		<div class="break"></div>

</div>


<div class="tiles">
<?php
	
	$search = postvar('search');
	if(!$_POST){
		$banners = new \obj\sql;
		$sql = $banners->seek("*","midia.portfolio2","draft='no'","order by id desc");
	}else{
		$banners = new \obj\sql;
		$sql = $banners->seek("*","midia.portfolio2","title ilike '%$search%' OR content ilike '%$search%' OR tags ilike '%$search%')AND(draft='no' ","order by id desc");
	}

	if($sql->num<1) echo '<p class="align-center font-big neutral-color padding-super">Nenhuma peça foi encontrada!</p>';
	else
	foreach($sql->rows as $row){ 

	$clear = str_replace(array("font-size","font-family"),array("erase-faces","erase-faces"),$row['content']);

	echo '<div class="size-33 portfolio-cell rounded">
				<div class="relative overflow-hidden z-1 rounded cursor-pointer shadowed" style="height:200px; overflow:hidden; margin:15px;">'	;

	if($row['link']!=null){

		#Youtube Preview
		$arrlink = explode(";",$row['link']);
		$arrtitle = explode(";",$row['title_video']);
		$arrdesc = explode(";",$row['legend_video']);
		$videos = '';

		foreach($arrlink as $index=>$value){
			if($index==0){
				$vid = youtubeVideoId($value);
				$vidImg = youtubeThumb($vid,$width="100%",$height="auto","hq",'class="rounded" style=""');
//				echo $vidImg;
				echo '<span class="inline rounded shadow-hover" style="width:100%; height:200px; overflow:hidden; background:white url(http://i2.ytimg.com/vi/'.$vid.'/hqdefault.jpg) no-repeat center;"></span>';
			}
																																																																																																																																																																																																						}
	}else{							

		$x = explode(";",$row['images']);														
		$xlength = count($x);
		if($xlength>1)
//		echo '<img src="'.IMG_THUMB.$x[0].'" class="rounded" width="100%"/>';
		echo '<span class="inline rounded shadow-hover" style="width:100%; height:200px; overflow:hidden; background:white url('.IMG_THUMB.$x[0].') no-repeat center;"></span>';
		else
//		echo '<img src="'.$row['images'].'" class="rounded" width="100%"/>';
		echo '<span class="inline rounded shadow-hover" style="width:100%; height:200px; overflow:hidden; background:white url('.IMG_THUMB.$row['images'].') no-repeat center;"></span>';

	}
	
	echo '<p class="bottom-rounded relative dark absolute-bottom-left b main-legend" style="width:100%;"><span class="inline padding bottom-rounded">'.$row['title'].'</span></p>
					<div class="fullscreen-relative z-10 dark rounded shaded invisible">
						<p class="b margin">'.$row['title'].'</p>
						<div class="margin pt12">'.limitstr($clear,333).'</div>
						<a href="portifolio/'.$row['url'].'" class="inline absolute-bottom-right margin color-orange u">Clique e veja as peças &rang;</a>
					</div>
				</div>
			</div>';

}
?>
<div class="break"></div>
<br />
<div class="margin">
<?php 
if($sql->num>=1) 
	echo '<a name="fb_share" type="button_count" share_url="'.HOST.'portifolio.html" class="margin-left inline"></a>
		  <a href="https://twitter.com/share" class="twitter-share-button" data-url="'.HOST.'portifolio.html" data-text="Portifólio Agência HiP!" data-via="agenciahip">Tweet</a>';
?>
</div>
</div>

