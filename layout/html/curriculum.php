<br />
<?php

	if($_POST){
	$emaildestinatario = "talentos@hipweb.com.br"; 
	$assunto = "HiP! Você recebeu um novo curriculum!";
	$emailsender = "talentos@hipweb.com.br";
	$quebra_linha = "\n";
	$file = isset($_FILES['Anexo']) ? $_FILES['Anexo'] : array('name'=>null);
	$boundary = strtotime('NOW');
	   
	$headers = "From: Website HiP! <talentos@hipweb.com.br>\n";
	$headers .= "MIME-Version: 1.0\n";

	$msg = '';

	$x = explode(".",$file['name']);
	$ext = array('doc','docx','jpg','png','pdf',);

	if(!in_array($x[1],$ext)) $file['name']=null;

	if($file['name']!=null){


		$headers .= "Content-Type: multipart/mixed; boundary=\"" . $boundary . "\"\n";
		$msg .= "--" . $boundary . "\n";
		$msg = "--" . $boundary . "\n";
		$msg .= "Content-Type: text/html; charset=\"utf-8\"\n";
//		$msg .= "Content-Transfer-Encoding: quoted-printable\n\n"; 
	
	}else{
	$headers .= "Content-Type: text/html; charset=\"utf-8\"\n";

	}



				$html = null;		
				foreach($_POST as $index=>$value){
					$html .= "<p>".$index.": ";		
					$html .= '<strong>'.$value.'</strong></p>';		
				}

				$msg .= '
					<html>
						<body style="background:#db812e;">						
							<div style="padding:5px; background:#db812e;">
								<table style="border:none; margin:5px; width:47px; height:72px; background-image:url('.HOST.'layout/img/hip-bottom.png); background-position:top; background-repeat:no-repeat;"><tr><td style="margin:5px; width:47px; line-height:40px; border:none;">&nbsp;</td></tr></table>
								<div style="padding:5px; background:#fff;">
									<p style="margin:5px 0 5px 0; font-size:22px;">Novo Curriculum</p>	
							';
	
									$msg .= $html;

									if($file['name']!=null) $msg.= '<p style="color:#db812e;">Contém 1 anexo!</p>';
					$msg .= '
								</div>
							</div>
						</body>
					</html>'.$quebra_linha;



	 

	if($file['name']!=null){
	 
		$msg .= "--" . $boundary . "\n";
		$msg .= "Content-Transfer-Encoding: base64\n";
		$msg .= "Content-Disposition: attachment; filename=\"".$file['name']."\"\n\n";

		ob_start();
		   readfile($file['tmp_name']);
		   $enc = ob_get_contents();
		ob_end_clean();

		$msg_temp = base64_encode($enc). "\n";
		$tmp[1] = strlen($msg_temp);
		$tmp[2] = ceil($tmp[1]/76);

		for ($b = 0; $b <= $tmp[2]; $b++) {
			$tmp[3] = $b * 76;
			$msg .= substr($msg_temp, $tmp[3], 76) . "\n";
		}

	//	unset($msg_temp, $tmp, $enc);

	}	

	if(!mail($emaildestinatario, $assunto, $msg, $headers ,"-r".$emailsender)){ // Se for Postfix
		$headers .= "Return-Path: " . $emailsender . $quebra_linha; // Se "não for Postfix"
		mail($emaildestinatario, $assunto, $msg, $headers );
	}

	echo '';

}

?>
<div class="tiles">
	<div class="size-70">
		<div class="padding" template="main_menu">
			<h1>Seu curriculum foi enviado com sucesso!</h1>
			  <h2>Obrigado pelo interesse.</h2>
			  <br />
			  <p>Atenciosanmente,</p>
			  <p class="pt18">Equipe HiP! Comunicação</p>		</div>
	</div>
	<div class="size-30">
		<div class="padding" template="main_products">
			<form action="resultados-da-busca.html" method="post" name="searchForm">
				<input type="text" name="search" class="rounded shaded padding5" style="width:220px;"/>
				<a class="hover margin-left float-right" onclick="document.searchForm.submit();"><img src="layout/img/search.jpg" /></a>
			</form>
			<br />
			<br />
			<h1 class="pt22">Siga a HiP!</h1>
			<br />
			<a href="http://www.facebook.com/hipcomunicacao" target="_blank"><img src="layout/img/facebook.jpg" width="28"/></a>
			<a href="https://twitter.com/agenciahip" target="_blank"><img src="layout/img/twitter.jpg" width="28"/></a>
			<a href="http://www.youtube.com/user/agenciahip" target="_blank"><img src="layout/img/youtube.jpg" width="28"/></a>
			<a href="https://soundcloud.com/hipcomunicacao" target="_blank"><img src="layout/img/cloud.jpg" width="28"/></a>
		</div>
	</div>
	<div class="break"></div>	
</div>
