//LOAD

$(document).ajaxComplete(function(){
	try{
		FB.XFBML.parse(); 
	}catch(ex){}
});


$(document).ready(function(){


	UI.Banner("div[template=main_banner]",990,250,4000);

	$(".portfolio-cell").mouseenter(function(){

		$(".main-legend",this).fadeOut(100,'easeOutCirc');
		$(".fullscreen-relative",this).fadeIn(200,'easeInCirc');

	}).mouseleave(function(){

		$(".fullscreen-relative",this).fadeOut(100,'easeInCirc');
		$(".main-legend").fadeIn(200,'easeInCirc');

	});

	$("select").wrap('<span class="inline shaded rounded"></span>');


	$("textarea,input").focus(function(){
		$(this).animate({backgroundColor:"#dddddd"},500);
	});

	$("select").focus(function(){
		$(this).animate({backgroundColor:"#dddddd"},500);
	});

	$("input[name=search]").focus(function(){
		$(this).animate({backgroundColor:"#ffffff"},500);
	});

	$("#main-loader").hide(0);
	$("#main-pages").show(0);

	
  $(window).scroll(function() { 
	var v = $('body').scrollTop();
	if(v>150) $(".go-top").fadeIn(); else $(".go-top").fadeOut();
  });


	$(document).keydown(function(e) {
		if (e.keyCode == 27) {

			$('[template=portfolio]').html(''); $('#portfolio-lite-box').slideUp(250,'easeOutCirc');

		}
	});

	$(document).keydown(function(e) {
		if (e.keyCode == 37) {
				liteboxPrev();
		}
	});

	$(document).keydown(function(e) {
		if (e.keyCode == 39) {

			liteboxNext();
		}
	});




});






function goToPage(jquerySelectorTemplate,page){

			$(jquerySelectorTemplate).html("<p style='padding:100px 0 100px 0;' class='align-center'>Aguarde, carregando ...</p>");
			$.ajax({
			  url: "layout/php/writeTemplate.php?template=main_news&offset="+page,
			  context: document.body,
			  success: function(msg){ 
				$(jquerySelectorTemplate).html(msg); 

					 $('html, body').animate({
						 scrollTop: $(jquerySelectorTemplate).offset().top
					 }, 500);
			  }
			});
}

function goTop(){

	 $('html, body').animate({
		 scrollTop: $("#main-pages").offset().top
	 }, 500);

}
/* functions */

function navNews(jquerySelectorTemplate,page){

			$(jquerySelectorTemplate).html("<p style='padding:100px 0 100px 0;' class='align-center'>Aguarde, carregando ...</p>");
			$.ajax({
			  url: "layout/php/writeTemplate.php?template=read_news&offset="+page,
			  context: document.body,
			  success: function(msg){ 
				$(jquerySelectorTemplate).html(msg); 

					 $('html, body').animate({
						 scrollTop: $(jquerySelectorTemplate).offset().top
					 }, 500);
			  }
			});
}


//POST FORM
function postform(){


	var er = RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);

	$("#postform").hide();
	var nome = $("input[name=Nome]").val();
	var email = $("input[name=Email]").val();
	var site = $("input[name=Site]").val();
	var assunto = $("input[name=Assunto]").val();
	var mensagem = $("textarea[name=Mensagem]").val();

	
	var err = 0;
	if(nome==''){ $("input[name=Nome]").animate({backgroundColor:"#f68121"},500);  err=err+1;  }else{ $("input[name=Nome]").animate({backgroundColor:"#c6f7b7"},500); }
	if(er.test(email) == false){ $("input[name=Email]").animate({backgroundColor:"#f68121"},500); err=err+1; }else{ $("input[name=Email]").animate({backgroundColor:"#c6f7b7"},500); }
	if(assunto==''){ $("input[name=Assunto]").animate({backgroundColor:"#f68121"},500); err=err+1; }else{ $("input[name=Assunto]").animate({backgroundColor:"#c6f7b7"},500); }
	if(mensagem==''){ $("textarea[name=Mensagem]").animate({backgroundColor:"#f68121"},500); err=err+1; }else{ $("textarea[name=Mensagem]").animate({backgroundColor:"#c6f7b7"},500); }

	if(err>0){ 

		$("#postform").show(); return(false); 

	}else{

		$.ajax({
		  url: "layout/php/ajax.php?page=postform&Nome="+nome+"&Email="+email+"&Assunto="+assunto+"&Mensagem="+mensagem,
		  context: document.body
		}).done(function(msg) { $("div[template=contact_form]").html(msg); return (true); });

	}	

}

//POST FORM
function showPorfolio(ID){

		var y = window.screen.height;
		$("#portfolio-lite-box").slideDown(250,'easeInCirc');
		var x = $("#lite-box-container").css('width');

		$.ajax({
		  url: "layout/php/writeTemplate.php?template=show-portfolio&id="+ID,
		  context: document.body
		}).done(function(msg) { 
			$("div[template=portfolio]").html(msg); 
			$(".pieces:nth(0)").show(0).addClass('selected');
			$("#lite-box-container").css({height:y-320});
			$(".pieces img").css({maxHeight:y-320,maxWidth:x});
			$(".pieces iframe").css({height:y-320,width:x});
			$(".nav-next,.nav-prev").css({top:"46%"});
		});


}


function validateForm(){


	var er = RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);

	$("#postform").hide();
	var nome = $("input[name=Nome]").val();
	var telefone = $("input[name=Telefone]").val();
	var cidade = $("input[name=Cidade]").val();
	var estado = $("select[name=Estado]").val();

	var sexo = $("select[name=Sexo]").val();
	var nascimento = $("input[name=Nascimento]").val();

	var cargo = $("select[name=Cargo]").val();

	var email = $("input[name=Email]").val();
	var anexo = $("input[name=Anexo]").val();


	
	var err = 0;
	if(nome==''){ $("input[name=Nome]").animate({backgroundColor:"#f68121"},500);  err=err+1;  }else{ $("input[name=Nome]").animate({backgroundColor:"#c6f7b7"},500); }
	if(telefone==''){ $("input[name=Telefone]").animate({backgroundColor:"#f68121"},500);  err=err+1;  }else{ $("input[name=Telefone]").animate({backgroundColor:"#c6f7b7"},500); }
	if(cidade==''){ $("input[name=Cidade]").animate({backgroundColor:"#f68121"},500);  err=err+1;  }else{ $("input[name=Cidade]").animate({backgroundColor:"#c6f7b7"},500); }
	if(estado==''){ $("select[name=Estado]").animate({backgroundColor:"#f68121"},500);  err=err+1;  }else{ $("select[name=Estado]").animate({backgroundColor:"#c6f7b7"},500); }
	if(sexo==''){ $("select[name=Sexo]").animate({backgroundColor:"#f68121"},500);  err=err+1;  }else{ $("input[name=Sexo]").animate({backgroundColor:"#c6f7b7"},500); }
	if(cargo==''){ $("select[name=Cargo]").animate({backgroundColor:"#f68121"},500);  err=err+1;  }else{ $("input[name=Cargo]").animate({backgroundColor:"#c6f7b7"},500); }
	if(nascimento==''){ $("input[name=Nascimento]").animate({backgroundColor:"#f68121"},500);  err=err+1;  }else{ $("input[name=Nascimento]").animate({backgroundColor:"#c6f7b7"},500); }

	if(er.test(email) == false){ $("input[name=Email]").animate({backgroundColor:"#f68121"},500); err=err+1; }else{ $("input[name=Email]").animate({backgroundColor:"#c6f7b7"},500); }
	if(anexo==''){ $("input[name=Anexo]").animate({backgroundColor:"#f68121"},500);  err=err+1;  }else{ $("input[name=Anexo]").animate({backgroundColor:"#c6f7b7"},500); }

	if(err>0){ 

		$("#postform").show(); return(false); 

	}else{

		document.myform.action='concluido.html'; 
		document.myform.submit();
	}

}


function litebox(){

	var v = $("body").css("height");
			$(".images-viewer").css({height:v}).fadeIn(200,"easeInCirc");
			$(".close-bt").fadeIn();
}

function liteboxExit(){

			$(".images-viewer").fadeOut(200,"easeInCirc");
			$(".close-bt").hide();
}

function liteboxNext(){

//	litebox();

	var l = $('.pieces').size();
	var i = $('.pieces.selected').index();
	$('.pieces').hide(0); 
	$('.pieces:nth('+i+')').fadeIn(0).addClass('selected');

	$('.pieces').hide(0).removeClass('selected'); 

	if(i<l-1){
		$('.pieces:nth('+(i+1)+')').fadeIn(0).addClass('selected');
	}else{
		$('.pieces:nth(0)').fadeIn(0).addClass('selected');
	}
}

function liteboxPrev(){

//	litebox();

	var l = $('.pieces').size();
	var i = $('.pieces.selected').index();

	$('.pieces').hide(0); 
	$('.pieces:nth('+i+')').fadeIn(0).addClass('selected');

	$('.pieces').hide(0).removeClass('selected'); 

	if(i>=1){
		$('.pieces:nth('+(i-1)+')').fadeIn(0).addClass('selected');
	}else{
		$('.pieces:nth('+(l-1)+')').fadeIn(0).addClass('selected');
	}
}


//Twitter
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");

(function($) {
    $.fn.hasScrollBar = function() {
         if(this.get(0).scrollHeight > this.height()) $('.go-top').hide(); else $('.go-top').show(); return this;
    }
})(jQuery);





/* jmask */

/*
	Masked Input plugin for jQuery
	Copyright (c) 2007-2013 Josh Bush (digitalbush.com)
	Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
	Version: 1.3.1
*/
(function(e){function t(){var e=document.createElement("input"),t="onpaste";return e.setAttribute(t,""),"function"==typeof e[t]?"paste":"input"}var n,a=t()+".mask",r=navigator.userAgent,i=/iphone/i.test(r),o=/android/i.test(r);e.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},dataName:"rawMaskFn",placeholder:"_"},e.fn.extend({caret:function(e,t){var n;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof e?(t="number"==typeof t?t:e,this.each(function(){this.setSelectionRange?this.setSelectionRange(e,t):this.createTextRange&&(n=this.createTextRange(),n.collapse(!0),n.moveEnd("character",t),n.moveStart("character",e),n.select())})):(this[0].setSelectionRange?(e=this[0].selectionStart,t=this[0].selectionEnd):document.selection&&document.selection.createRange&&(n=document.selection.createRange(),e=0-n.duplicate().moveStart("character",-1e5),t=e+n.text.length),{begin:e,end:t})},unmask:function(){return this.trigger("unmask")},mask:function(t,r){var c,l,s,u,f,h;return!t&&this.length>0?(c=e(this[0]),c.data(e.mask.dataName)()):(r=e.extend({placeholder:e.mask.placeholder,completed:null},r),l=e.mask.definitions,s=[],u=h=t.length,f=null,e.each(t.split(""),function(e,t){"?"==t?(h--,u=e):l[t]?(s.push(RegExp(l[t])),null===f&&(f=s.length-1)):s.push(null)}),this.trigger("unmask").each(function(){function c(e){for(;h>++e&&!s[e];);return e}function d(e){for(;--e>=0&&!s[e];);return e}function m(e,t){var n,a;if(!(0>e)){for(n=e,a=c(t);h>n;n++)if(s[n]){if(!(h>a&&s[n].test(R[a])))break;R[n]=R[a],R[a]=r.placeholder,a=c(a)}b(),x.caret(Math.max(f,e))}}function p(e){var t,n,a,i;for(t=e,n=r.placeholder;h>t;t++)if(s[t]){if(a=c(t),i=R[t],R[t]=n,!(h>a&&s[a].test(i)))break;n=i}}function g(e){var t,n,a,r=e.which;8===r||46===r||i&&127===r?(t=x.caret(),n=t.begin,a=t.end,0===a-n&&(n=46!==r?d(n):a=c(n-1),a=46===r?c(a):a),k(n,a),m(n,a-1),e.preventDefault()):27==r&&(x.val(S),x.caret(0,y()),e.preventDefault())}function v(t){var n,a,i,l=t.which,u=x.caret();t.ctrlKey||t.altKey||t.metaKey||32>l||l&&(0!==u.end-u.begin&&(k(u.begin,u.end),m(u.begin,u.end-1)),n=c(u.begin-1),h>n&&(a=String.fromCharCode(l),s[n].test(a)&&(p(n),R[n]=a,b(),i=c(n),o?setTimeout(e.proxy(e.fn.caret,x,i),0):x.caret(i),r.completed&&i>=h&&r.completed.call(x))),t.preventDefault())}function k(e,t){var n;for(n=e;t>n&&h>n;n++)s[n]&&(R[n]=r.placeholder)}function b(){x.val(R.join(""))}function y(e){var t,n,a=x.val(),i=-1;for(t=0,pos=0;h>t;t++)if(s[t]){for(R[t]=r.placeholder;pos++<a.length;)if(n=a.charAt(pos-1),s[t].test(n)){R[t]=n,i=t;break}if(pos>a.length)break}else R[t]===a.charAt(pos)&&t!==u&&(pos++,i=t);return e?b():u>i+1?(x.val(""),k(0,h)):(b(),x.val(x.val().substring(0,i+1))),u?t:f}var x=e(this),R=e.map(t.split(""),function(e){return"?"!=e?l[e]?r.placeholder:e:void 0}),S=x.val();x.data(e.mask.dataName,function(){return e.map(R,function(e,t){return s[t]&&e!=r.placeholder?e:null}).join("")}),x.attr("readonly")||x.one("unmask",function(){x.unbind(".mask").removeData(e.mask.dataName)}).bind("focus.mask",function(){clearTimeout(n);var e;S=x.val(),e=y(),n=setTimeout(function(){b(),e==t.length?x.caret(0,e):x.caret(e)},10)}).bind("blur.mask",function(){y(),x.val()!=S&&x.change()}).bind("keydown.mask",g).bind("keypress.mask",v).bind(a,function(){setTimeout(function(){var e=y(!0);x.caret(e),r.completed&&e==x.val().length&&r.completed.call(x)},0)}),y()}))}})})(jQuery);

$("#fone").mask("(99) 9999-9999");
$("#data").mask("99/99/9999");


function in_array(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

function valida_extensao(campo){

	var permitidos = new Array();
	 permitidos[0] = "doc";
	 permitidos[1] = "docx";
	 permitidos[2] = "pdf";
	 permitidos[3] = "jpg";
	 permitidos[3] = "png";

    var ext = $(campo).val().split(".")[1].toLowerCase();
        
        if(!this.in_array(ext, permitidos)){
            alert("Arquivo não permitido: "+ext+". São permitidos apenas, *.doc,*.docx,*.pdf,*.jpg,*.png");
            $(campo).val("").empty();
        }
}
