<?php require "vendor/autoload.php"; ?>
<!DOCTYPE HTML>
<html lang="bt-br">
<head>
<base href="<?=HOST?>" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<meta name="title" content="HiP!" />
<meta name="url" content="<?=HOST?>" />
<meta name="description" content="" />

<meta name="generator" content="Suhdo Tecnologia">		
<meta name="company" content="Suhdo Tecnologia"/>
<meta name="reply-to" content="marcio@suhdo.com">	
<meta name="rating" content="Gerar resultados e lucratividade através de soluções em comunicação focadas nas necessidades dos nossos parceiros">		
<meta name="keywords" content="publicidade, propaganda, vt, radio, tv, agencia, design, uberlandia, comunicacao, resultados expressivos" />

<?php
$url = \obj\info::url();
\obj\controler::facebookAPI($url->module);
?>

	<link rel="stylesheet" type="text/css" href="layout/css/fluid.suhdo.css" />
	<link rel="stylesheet" type="text/css" href="plugins/jquery/css/smoothness/jquery-ui-1.10.0.custom.min.css" />
	<link rel="stylesheet" type="text/css" href="layout/css/style.css" />

	<link rel="shortcut icon" href="layout/img/favicon.ico" />

	<title>Agência Sun</title>
</head>

<body>
		<div id="main-loader"><p class="padding-super align-center font-medium color-gray white">Carregando ...</p></div>
		<div id="main-pages" class="relative white">
			<div class="tiles-container " >
				<?php
				\obj\controler::pages(array("404.html"=>"404.php",
											""=>"home.php",
											"ler"=>"read.php"));
				?>
			</div>
			<a class="fixed-bottom-right margin hover go-top invisible" onclick="goTop();"><img src="layout/img/up.png" alt=""/></a>
		</div>
	<div id="fb-root"></div>
	<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>

	<script src="plugins/jquery/js/jquery-1.9.0.min.js"></script>
	<script src="plugins/jquery/js/jquery-ui-1.10.0.custom.min.js"></script>
	<script src="plugins/suhdo/js/responsive.js"></script>
	<script src="plugins/suhdo/js/controler.js"></script>
	<script src="plugins/suhdo/js/twitter.js"></script>
	<script src="layout/js/site.js"></script>


</body>
</html>
