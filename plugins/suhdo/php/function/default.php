<?php 

function settings($row){

	$std = new stdClass;

	$std->{"license"} = $row["license"];
	$std->{"domine"} = $row["domine"];
	$std->{"local_domine"} = $row["local_domine"];
	$std->{"owner"} = $row["id"];

	return $std;

}

/* VARS */


	function postarr($var=null){

		if(!is_array($_POST[$var])) return array(0=>$_POST[$var]);
		$v = isset($_POST[$var]) ? $_POST[$var] : null;
		return $v;

	}

	function postvar($var=null){

		#recive a POST submission
		$v = isset($_POST[$var]) ? $_POST[$var] : null;
		return @addslashes(trim($v));

	}

	function getvar($var=null){

	#recive a GET submission 	

		$v = isset($_GET[$var]) ? $_GET[$var] : null;
		return addslashes(trim($v));

	}

	function ajaxvar($var=null){

	#recive  data from ajax exclusively	

		$v = isset($_REQUEST[$var]) ? $_REQUEST[$var] : null;
		return addslashes(trim($v));

	}

	function arrayvar($var){

	#recive an array post 	

	    $v = isset($_POST[$var]) ? $_POST[$var] : null;
	    return array('values'=>$v,'size'=>count($v));

	}



	function makearray($slash,$string){

	#Break a formated sting into an array 	

		$x = explode($slash,$string);
		$length = count($x);

		return array("length"=>$length,"values"=>$x);		

	}

	function makestring($slash,$array){


		#Break a array object or a POST submission into a string 	

		$string = "";
		$length = count($array);

		if($length<1){

		$n = arrayvar($array);

			for($i=0;$i<$n['size'];$i++){
			    if($n['values'][$i]!="") $string.= addslashes($n['values'][$i]).$slash;
			}

		}else{
			for($i=0;$i<$length;$i++){
			    if($array[$i]!="") $string.= addslashes($array[$i]).$slash;
			}

		}

		return substr($string,0,-1);
	}


	#limit chars and cleat tags from text		
	function limitstr($str=NULL,$chars=30,$continue="..."){

		$string = strip_tags($str);
		$length = strlen($string);
		$sub = substr($string,0,$chars);
		if($length<=$chars) return trim($sub); else return trim($sub . $continue);

	}



	#youtube

	#youtube
    function youtubeVideoId($url="v=&"){

		$link = makearray("v=",$url);
	
		if($link["length"]>=1){

			if(!isset($link['values'][1])) return "ERROR-GET-YOUTUBE-VID";
			$video = makearray("&",$link['values'][1]); 

			if($video["length"]>=1){
				return $video['values'][0];
			}else{

				if(isset($link['values'][1])){
					return $link['values'][1];

				}else{
				$link = makearray("watch?v=",$url); 
				return $link['values'][1];

				}
			}
		}else{

			$link = makearray("watch?v=",$url); 
			if($link['length']==0) return "ERROR-GET-YOUTUBE-VID"; else return $link['values'][1];
		}
	}

/*    function youtubeVideoId($url="v=&"){

		$link = makearray("v=",$url);

		if($link["length"]>=1){

				
			$video = makearray("&",$link['values'][1]);

			if($video["length"]>=1){
				return $video['values'][0];
			}else{
				return $link['values'][1];
			}
		}else{
			echo FALSE;
		}
	}
*/
	function youtubePlayer($videoId=NULL,$width=420,$height="auto",$extra="allowfullscreen"){

		return '<iframe width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/'.$videoId.'?rel=0" frameborder="0" '.$extra.'></iframe>';

	}

	function youtubeThumb($videoId=NULL,$width=120,$height=90,$quality="hq",$tagsAttr=''){

		return '<img src="http://i1.ytimg.com/vi/'.$videoId.'/'.$quality.'default.jpg" width="'.$width.'" height="'.$height.'" alt="'.$videoId.'" '.$tagsAttr.' />';

	}
?>
