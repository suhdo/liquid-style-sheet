<?php

namespace obj;

class template{

	public function homepageNews($templateHtml='',$limitResults=1,$textBt='Continue Lendo &rang;',$activePages=true){

		$url = info::url();

		$module = isset($modules[$url->module]) ? $modules[$url->module] : "404.html";
		$offset = ajaxvar('offset');	
	
		$oset = $limitResults*$offset-$limitResults;
		switch($templateHtml){

			case 'preview-data-1':

				$banners = new \obj\sql;
				$sql = $banners->seek("*","homepage.news","draft='no'","order by id desc",$limitResults,$oset);
		
				if($sql->num<1) echo '<p class="align-center font-big neutral-color padding-super">Nenhuma Notícia foi adicionada ainda!</p>';
				else
				foreach($sql->rows as $row){

				$header = '<h1><a href="ler/'.$row['url'].'" class="hover inline">'.$row['title'].'</a></h1>
						   <h2 class="color-gray"><a href="ler/'.$row['url'].'" class="hover inline"><span class="color-gray">'.$row['subtitle'].'</span></a></h2>
						   <br />';
				$clear = str_replace(array("font-size","font-family"),array("erase-faces","erase-faces"),$row['content']);
				$content = '<div class="marginy pt14"><a href="ler/'.$row['url'].'" class="hover inline">'.limitstr($clear,333).'</a></div>';

				#Images Preview
				$arrimgs = explode(";",$row['images']);
				$arrlegend = explode(";",$row['legend_images']);
				$images = '';
				$images .= '<div class="marginy img-preview inline" style="width:100%; overflow:hidden; height:250px;">';

						foreach($arrimgs as $index=>$value){
							if($index==0){
								 $images .= '<a href="ler/'.$row['url'].'" class="shadow-hover inline rounded" style="width:100%; overflow:hidden; height:250px; background:white url('.IMG_DEFAULT.$value.') no-repeat center;">&nbsp;</a>';
							}
						}

				$images .= '</div>
							<br />';

				#Youtube Preview
				$arrlink = explode(";",$row['link']);
				$arrtitle = explode(";",$row['title_video']);
				$arrdesc = explode(";",$row['legend_video']);
				$videos = '';
				foreach($arrlink as $index=>$value){
					$vid = youtubeVideoId($value);
					$vidImg = youtubeThumb($vid,$width="100%",$height="auto","hq",'style=""');
					if($index==0)
					$videos .= '<div class="marginy">
									<div class="margin-top margin-bottom shadow-hover" style="overflow:hidden; height:250px;"><a href="ler/'.$row['url'].'" class="shadow-hover">'.$vidImg.'</a></div>
									<p class="break"></p>
									<br />
								</div>';
				}

				echo $header;

				switch($row['post_model']){
		
					default:

						echo $content;

					break;

					case "images-1":

						echo $images;
						echo $content;

					break;

					case "images-2":

						echo $images;
						echo $content;

					break;

					case "videos-1":

						echo $videos;
						echo $content;

					break;

					case "videos-2":

						echo $videos;
						echo $content;

					break;


					case "full-1":

						echo $images;
						echo $content;
	
					break;

					case "full-2":

						echo $images;
						echo $content;

					break;

					case "full-3":

						echo $images;
						echo $content;
					break;

					case "full-4":

						echo $images;
						echo $content;
	
					break;

				}

					echo '<br />
		<p class="align-right"><a href="ler/'.$row['url'].'" class="font-medium b hover">'.$textBt.'</a></p>
		<br />
		<br />';
		}


		$bannersNum = new \obj\sql;
		$sqlNum = $bannersNum->seek("id","homepage.news","draft='no'","order by id desc",$limitResults,$offset);
		$totalPages = $sqlNum->num/$limitResults;
		$nextpage = $offset+1;
		if($nextpage>=$totalPages) $nextpage = $totalPages;
		if($nextpage<2) $nextpage = 2;
		if($offset<1) $offset=1;
		if($offset>$totalPages) $offset=$totalPages;
		if($sqlNum->num>10) $n = 10; else $n = $sqlNum->num;

		if($totalPages>1){

			echo '
			<p class="margin-top">';

				for($i=1;$i<=$n;$i++){
			
					if($i==$offset) $selected = 'selected'; else $selected = ''; 
					echo '<a href="javascript:void(0);" class="inline b rounded shaded padding5 pages margin5-right '.$selected.'" bt-page="'.$i.'" onclick="goToPage(\'div[template=main_news]\',\''.$i.'\');">'.$i.'</a>';
				}
					echo '<a href="javascript:void(0);" class="inline b rounded shaded padding5 pages" bt-page="last" onclick="goToPage(\'div[template=main_news]\',\''.($nextpage).'\');">&rang;&rang;</a>
			</p>';

		}

	
			break;

			case "read-full": 

				$banners = new \obj\sql;
				$sql = $banners->seek("*","homepage.news","draft='no' AND url='".$url->property."'","order by id desc",$limitResults,$oset);
		
				if($sql->num<1) echo '<p class="align-center font-big neutral-color padding-super">Nenhuma Notícia foi adicionada ainda!</p>';
				else
				foreach($sql->rows as $row){

				$header = '<h1>'.$row['title'].'</h1>
						   <h2 class="color-gray">'.$row['subtitle'].'</h2>
						   <br />';

				$clear = str_replace(array("font-size","font-family"),array("erase-faces","erase-faces"),$row['content']);
				$content = '<div class="marginy justify pt14">'.$clear.'</div>';

				#Images Preview
				$arrimgs = explode(";",$row['images']);
				$arrlegend = explode(";",$row['legend_images']);
				$images = '';
				$images .= '<br />
								<div class="relative on-top marginy align-center">';



				foreach($arrimgs as $index=>$value){
					if($arrlegend[$index]!=null) $showlegend = ''; else $showlegend = 'invisible';
						$images .= '<div class="relative marginy shadowed">
										<img src="'.IMG_DEFAULT.$value.'" class="" width="100%"/>
										<p class="'.$showlegend.' absolute-bottom-left z-2 dark" style="width:100%;">
											<span class="inline padding">'.$arrlegend[$index].'</span>
										</p>
									</div><br />';
				}

				$images .= '<a href="javascript:void(0);" class="close-bt absolute-top-right font-big color-gray inline margin-left" style="right:10px; display:none;" onclick="liteboxExit();">x</a>';

				$images .= '</div>';

				$limg = count($arrimgs);

/*
				if($limg>1){

					$images .= '<div class="margin-top relative on-top">';
					foreach($arrimgs as $index=>$value){
						$images .= '<a class="hover" onclick="litebox(); $(\'.portfolio-imgs\').hide(0); $(\'.portfolio-imgs:nth(\'+'.$index.'+\')\').fadeIn(0).addClass(\'selected\');"><img src="'.IMG_ICO.$value.'" class="" style="width:16,666666667%;"/></a>';
					}
				$images .= '</div>';

				}
*/
				$images .= '<br />';


				#Youtube Preview
				$arrlink = explode(";",$row['link']);
				$arrtitle = explode(";",$row['title_video']);
				$arrdesc = explode(";",$row['legend_video']);
				$videos = '';
				foreach($arrlink as $index=>$value){
					$vid = youtubeVideoId($value);
					$vidImg = youtubePlayer($vid,"100%",400,"hq");

					$videos .= '<div class="marginy">
									<p class="margin-top margin-bottom">'.$vidImg.'</p>
									<p class="margin-top margin-bottom b">'.$arrtitle[$index].'</p>
									<p class="margin-top margin-bottom i">'.$arrdesc[$index].'</p>
									<p class="break"></p>
									<br />
								</div>';
				}

				echo $header;

				switch($row['post_model']){
		
					default:

						echo $content;

					break;

					case "images-1":

						echo $content;
						echo $images;

					break;

					case "images-2":

						echo $images;
						echo $content;

					break;

					case "videos-1":

						echo $content;
						echo $videos;

					break;

					case "videos-2":

						echo $videos;
						echo $content;

					break;


					case "full-1":

						echo $images;
						echo $content;
						echo $videos;
	
					break;

					case "full-2":

						echo $videos;
						echo $content;
						echo $images;
	
					break;

					case "full-3":

						echo $images;
						echo $videos;
						echo $content;
	
					break;

					case "full-4":

						echo $content;
						echo $images;
						echo $videos;
	
					break;
					default:

						echo $content;

					break;

					case "images-1":

						echo $content;
						echo $images;

					break;

					case "images-2":

						echo "<br />".$images;
						echo $content;

					break;

					case "videos-1":

						echo $content;
						echo $videos;

					break;

					case "videos-2":

						echo $videos;
						echo $content;

					break;


					case "full-1":

						echo $images;
						echo $content;
						echo $videos;
	
					break;

					case "full-2":

						echo $videos;
						echo $content;
						echo $images;
	
					break;

					case "full-3":

						echo $images;
						echo $videos;
						echo $content;
	
					break;

					case "full-4":

						echo $content;
						echo $images;
						echo $videos;
	
					break;

				}



			echo '<br />
					<a name="fb_share" type="button_count" share_url="'.HOST.'ler/'.$row['url'].'" class="inline" data-locale="pt_BR"></a>
					<a href="https://twitter.com/share" class="twitter-share-button" data-url="'.HOST.'ler/'.$row['url'].'" data-text="'.limitstr($row['title'],44).'" data-via="agenciahip">Tweet</a>
					<br />
					<br />
					<div class="fb-comments" data-href="'.HOST.'ler/'.$row['url'].'" data-width="680" data-num-posts="10" data-locale="pt_BR"></div>
				  ';

		$prevNum = new \obj\sql;
		$prev = $prevNum->seek("url","homepage.news","id<".$row['id'],"order by id desc",1,0);

				}


//				echo '<br /><div class="marginy i">'.$row['complement'].'</div>';

		echo '<br />
		<br />
		<p class="margin-top">';


		$nextNum = new \obj\sql;
		$next = $nextNum->seek("url","homepage.news","id>".$row['id'],"order by id desc",1,0);

		if($prev->num==1) foreach($prev->rows as $url)  echo '<a href="ler/'.$url['url'].'" class="float-left inline b rounded shaded padding5 pages" bt-page="last">&lang;&lang; Mais Antigas</a>';
		if($next->num==1) foreach($next->rows as $url)  echo '<a href="ler/'.$url['url'].'" class="float-right  inline b rounded shaded padding5 pages" bt-page="last">Mais Recentes &rang;&rang;</a>';

		echo '</p>';

			break;
		}



	}





	public function midiaPortfolio2($templateHtml='',$limitResults=1,$textBt='Continue Lendo &rang;',$activePages=true){

		$url = info::url();

		$module = isset($modules[$url->module]) ? $modules[$url->module] : "404.html";
		$offset = ajaxvar('offset');	
	
		$oset = $limitResults*$offset-$limitResults;
		switch($templateHtml){

			case 'preview-data-1':


			break;

			case "read-full": 

				$banners = new \obj\sql;
				$sql = $banners->seek("*","midia.portfolio2","draft='no' AND url='".$url->property."'","order by id desc",$limitResults,$oset);
		
				if($sql->num<1) echo '<p class="align-center font-big neutral-color padding-super">Esta peça não existe ou foi removida!</p>';
				else
				foreach($sql->rows as $row){

				$header = '<h1>'.$row['title'].'</h1>
						   <h2 class="color-gray">'.$row['subtitle'].'</h2>
						   <br />';

				$clear = str_replace(array("font-size","font-family"),array("erase-faces","erase-faces"),$row['content']);
				$content = '<div class="marginy justify pt14">'.$clear.'</div>';

				#Images Preview
				$arrimgs = explode(";",$row['images']);
				$arrlegend = explode(";",$row['legend_images']);
				$images = '';
				$images .= '<br />
								<div class="relative on-top marginy align-center">';


				foreach($arrimgs as $index=>$value){
					if($arrlegend[$index]!=null) $showlegend = ''; else $showlegend = 'invisible';
						$images .= '<div class="relative marginy">
										<img src="'.IMG_DEFAULT.$value.'" class="" width="100%"/>
										<p class="'.$showlegend.' absolute-bottom-left z-2 dark" style="width:100%;">
											<span class="inline padding">'.$arrlegend[$index].'</span>
										</p>
									</div> <br />';
				}

				$images .= '<a href="javascript:void(0);" class="close-bt absolute-top-right font-big color-gray inline margin-left" style="right:10px; display:none;" onclick="liteboxExit();">x</a>';
				$images .= '</div>';

				$limg = count($arrimgs);
/*
				if($limg>1){

					$images .= '<div class="margin-top relative on-top">';
					foreach($arrimgs as $index=>$value){
						$images .= '<a class="hover" onclick="litebox(); $(\'.portfolio-imgs\').hide(0); $(\'.portfolio-imgs:nth(\'+'.$index.'+\')\').fadeIn(0).addClass(\'selected\');"><img src="'.IMG_ICO.$value.'" class="" style="width:16,666666667%;"/></a>';
					}
				$images .= '</div>';

				}
*/
				$images .= '<br />';


				#Youtube Preview
				$arrlink = explode(";",$row['link']);
				$arrtitle = explode(";",$row['title_video']);
				$arrdesc = explode(";",$row['legend_video']);
				$videos = '';
				foreach($arrlink as $index=>$value){
					$vid = youtubeVideoId($value);
					$vidImg = youtubePlayer($vid,"100%",400,"hq");

					$videos .= '<div class="marginy">
									<p class="margin-top margin-bottom">'.$vidImg.'</p>
									<p class="margin-top margin-bottom b">'.$arrtitle[$index].'</p>
									<p class="margin-top margin-bottom i">'.$arrdesc[$index].'</p>
									<p class="break"></p>
									<br />
								</div>';
				}

				echo $header;

				switch($row['post_model']){
		
					default:

						echo $content;

					break;

					case "images-1":

						echo $content;
						echo $images;

					break;

					case "images-2":

						echo $images;
						echo $content;

					break;

					case "videos-1":

						echo $content;
						echo $videos;

					break;

					case "videos-2":

						echo $videos;
						echo $content;

					break;


					case "full-1":

						echo $images;
						echo $content;
						echo $videos;
	
					break;

					case "full-2":

						echo $videos;
						echo $content;
						echo $images;
	
					break;

					case "full-3":

						echo $images;
						echo $videos;
						echo $content;
	
					break;

					case "full-4":

						echo $content;
						echo $images;
						echo $videos;
	
					break;
					default:

						echo $content;

					break;

					case "images-1":

						echo $content;
						echo $images;

					break;

					case "images-2":

						echo "<br />".$images;
						echo $content;

					break;

					case "videos-1":

						echo $content;
						echo $videos;

					break;

					case "videos-2":

						echo $videos;
						echo $content;

					break;


					case "full-1":

						echo $images;
						echo $content;
						echo $videos;
	
					break;

					case "full-2":

						echo $videos;
						echo $content;
						echo $images;
	
					break;

					case "full-3":

						echo $images;
						echo $videos;
						echo $content;
	
					break;

					case "full-4":

						echo $content;
						echo $images;
						echo $videos;
	
					break;

				}



			echo '<br />
					<a name="fb_share" type="button_count" share_url="'.HOST.'portifolio/'.$row['url'].'" class="inline" data-locale="pt_BR"></a>
					<a href="https://twitter.com/share" class="twitter-share-button" data-url="'.HOST.'portifolio/'.$row['url'].'" data-text="'.limitstr($row['title'],44).'" data-via="agenciahip">Tweet</a>
					<br />
					<br />
					<div class="fb-comments" data-href="'.HOST.'portifolio/'.$row['url'].'" data-width="680" data-num-posts="10" data-locale="pt_BR"></div>
				  ';

		$prevNum = new \obj\sql;
		
			$prev = $prevNum->seek("url","midia.portfolio2","id<".$row['id'],"order by id desc",1,0);

		}


//				echo '<br /><div class="marginy i">'.$row['complement'].'</div>';

		echo '<br />
		<br />
			 <p class="margin-top">';


		$nextNum = new \obj\sql;
		$next = $nextNum->seek("url","midia.portfolio2","id>".$row['id'],"order by id desc",1,0);

		if($prev->num==1) foreach($prev->rows as $url)  echo '<a href="portifolio/'.$url['url'].'" class="float-left inline b rounded shaded padding5 pages" bt-page="first">&lang;&lang; Peça Anterior</a>';
		if($next->num==1) foreach($next->rows as $url)  echo '<a href="portifolio/'.$url['url'].'" class="float-right  inline b rounded shaded padding5 pages" bt-page="last">Peça Seguinte &rang;&rang;</a>';

		echo '</p>';

			break;
		}



	}

}


?>
