<?php

namespace obj;

class sql{

    public $connect;

	public function connect(){
		return $connect = new \PDO('pgsql:host=186.202.13.26;dbname=suhdo','suhdo','suhdo777');
	}

	//excute a sql command and returns a query object
	public static function query($sqlCmd=FALSE){

		if($sqlCmd==FALSE) return FALSE;

		$con = new sql;		
   	 	$con->connect();

		$pdo = $con->connect();

		return $pdo->query($sqlCmd);
	}


	//return an array name_assoc results of a sql one line query object
	public static function row($queryObj=FALSE){

		if($queryObj==FALSE) return;
		return $queryObj->fetch(\PDO::FETCH_ASSOC);

	}

	public function seek($fields,$table,$clause="id>0",$order="order by id asc",$limit=100,$offset=0){

		$std = new \stdClass;
		$sql = "SELECT $fields FROM $table WHERE($clause)AND(owner=".OWNER." AND stat='A') $order LIMIT $limit OFFSET $offset";
		$query = sql::query($sql);

		$std->rows = sql::rows($query);
		$std->num = sql::numrows($query);

		return $std;
	}

	//return nom lines of a sql query object
	public static function numrows($queryObj=FALSE){

		if($queryObj==FALSE) return FALSE;
		$rows = $queryObj->rowCount();
		return $rows;

	}


	//return an array name_assoc results of a sql query object
	public static function rows($queryObj=FALSE,$stdOutput=TRUE){

		if($queryObj==FALSE) return;

		$lines = array();
		$array = array();
		$r = array();
		$std = new \stdClass;
 
     		while($rows = $queryObj->fetchAll(\PDO::FETCH_ASSOC)){

			foreach($rows as $index=>$value){

				$array[$index] = $value;				
				$std->{$index} = $value;
			}

		}
		if($stdOutput==TRUE)
		return($std);
		else
		return($array);

	}

}
?>
