<?php

namespace obj;

class controler{

	public static function pages($modules=array()){

		$url = info::url();

		$module = isset($modules[$url->module]) ? $modules[$url->module] : "404.html";
				
		switch($url->module){

			default: 
				if(file_exists("layout/html/".$module)) 
					require "layout/html/".$module;
				else
					require "layout/html/404.html";

			break;
		}
	}


	public static function facebookAPI($module){

		$url = info::url();

		switch($module){

				case "":

				$banners = new \obj\sql;
				$sql = $banners->seek("*","homepage.news","draft='no'","order by id desc",3,0);

				foreach($sql->rows as $row){

					#Images Preview
					$arrimgs = explode(";",$row['images']);
					$arrlegend = explode(";",$row['legend_images']);

					echo '
					<meta property="og:site_name" content="..:: HiP! Comunicação | +55 34 3234-8207 ::.." />
					<meta property="og:title" content="'.$row['title'].'" />
					<meta property="og:url" content="'.HOST.'ler/'.$row['url'].'"/>
					<meta property="fb:admins" content="759782736" />
					<meta name="description" content="'.limitstr($row['content'],200).'" />
					';


					#Youtube Preview
					$arrlink = explode(";",$row['link']);
					$videos = '';
					foreach($arrlink as $index=>$value){
						$vid = youtubeVideoId($value);
						echo '<meta property="og:image" content="http://i4.ytimg.com/vi/'.$vid.'/hqdefault.jpg" />';
					}

					foreach($arrimgs as $index=>$value){
							if($arrimgs[$index]!="") $foto = IMG_THUMB.$arrimgs[$index]; else $foto = HOST.'layout/img/logo-hip-fb.jpg';
								echo '<meta property="og:image" content="'.$foto.'" />';
					}

				}
				break;


				case "universo-hip.html":

				$banners = new \obj\sql;
				$sql = $banners->seek("*","homepage.news","draft='no'","order by id desc",3,0);

				foreach($sql->rows as $row){

					#Images Preview
					$arrimgs = explode(";",$row['images']);
					$arrlegend = explode(";",$row['legend_images']);

					echo ' 
					<meta property="og:site_name" content="..:: HiP! Comunicação | +55 34 3234-8207 ::.." />
					<meta property="og:title" content="HiP! Comunicação - '.$row['title'].'" />
					<meta property="og:url" content="'.HOST.'ler/'.$row['url'].'"/>
					<meta property="fb:admins" content="759782736" />
					<meta name="description" content="'.limitstr($row['content'],200).'" />
					';


					#Youtube Preview
					$arrlink = explode(";",$row['link']);
					$videos = '';
					foreach($arrlink as $index=>$value){
						$vid = youtubeVideoId($value);
						echo '<meta property="og:image" content="http://i4.ytimg.com/vi/'.$vid.'/hqdefault.jpg" />';
					}

					foreach($arrimgs as $index=>$value){
							if($arrimgs[$index]!="") $foto = IMG_THUMB.$arrimgs[$index]; else $foto = HOST.'layout/img/logo-hip-fb.jpg';
								echo '<meta property="og:image" content="'.$foto.'" />';
					}

				}
				break;

				case "ler":

				$banners = new \obj\sql;
				$sql = $banners->seek("*","homepage.news","draft='no' AND url='".$url->property."'","order by id desc",1,0);

				foreach($sql->rows as $row){

					#Images Preview
					$arrimgs = explode(";",$row['images']);
					$arrlegend = explode(";",$row['legend_images']);

					echo ' 
					<meta property="og:site_name" content="..:: HiP! Comunicação | +55 34 3234-8207 ::.." />
					<meta property="og:title" content="'.$row['title'].'" />
					<meta property="og:url" content="'.HOST.'ler/'.$row['url'].'"/>
					<meta property="og:type" content="blog" />
					<meta property="fb:admins" content="759782736" />
					<meta name="description" content="'.limitstr($row['content'],200).'" />
					';


					#Youtube Preview
					$arrlink = explode(";",$row['link']);
					$videos = '';
					foreach($arrlink as $index=>$value){
						$vid = youtubeVideoId($value);
						echo '<meta property="og:image" content="http://i4.ytimg.com/vi/'.$vid.'/hqdefault.jpg" />';
					}

					foreach($arrimgs as $index=>$value){
							if($arrimgs[$index]!="") $foto = IMG_THUMB.$arrimgs[$index]; else $foto = HOST.'layout/img/logo-hip-fb.jpg';
								echo '<meta property="og:image" content="'.$foto.'" />';
					}

				}
				break;

				case "portifolio":

				$banners = new \obj\sql;
				$sql = $banners->seek("*","midia.portfolio2","url='".$url->property."'","order by id desc");

				foreach($sql->rows as $row){

					echo ' 
					<meta property="og:site_name" content="..:: HiP! Comunicação | +55 34 3234-8207 ::.." />
					<meta property="og:title" content="'.$row['title'].'" />
					<meta property="og:url" content="'.HOST.'portifolio/'.$row['url'].'"/>
					<meta property="og:type" content="blog" />
					<meta property="fb:admins" content="759782736" />
					<meta name="description" content="'.limitstr($row['content'],200).'" />';

					#Youtube Preview
					$arrlink = explode(";",$row['link']);
					$videos = '';
					foreach($arrlink as $index=>$value){
						$vid = youtubeVideoId($value);
						echo '<meta property="og:image" content="http://i4.ytimg.com/vi/'.$vid.'/hqdefault.jpg" />';
					}

					$arrimgs = explode(";",$row['images']);

					foreach($arrimgs as $index=>$value){
							if($arrimgs[$index]!=null) $foto = IMG_THUMB.$arrimgs[$index]; else $foto = HOST.'layout/img/logo-hip-fb.jpg';
								echo '<meta property="og:image" content="'.$foto.'" />';
					}



				}
				break;


				case "portifolio.html":

				$banners = new \obj\sql;
				$sql = $banners->seek("*","midia.portfolio2");
				$videos = '';

					echo ' 
					<meta property="og:site_name" content="..:: HiP! Comunicação | +55 34 3234-8207 ::.." />
					<meta property="og:title" content="Portifólio - Agência HiP!" />
					<meta property="og:url" content="portifolio.html/"/>
					<meta property="og:type" content="blog" />
					<meta property="fb:admins" content="759782736" />
					<meta name="description" content="Acessem nosso site e confiram algumas peças do nosso portifólio" />
					';

				foreach($sql->rows as $row){

					#Images Preview
					$arrimgs = explode(";",$row['images']);
					$arrlegend = explode(";",$row['legend_images']);


					#Youtube Preview
					$arrlink = explode(";",$row['link']);
					foreach($arrlink as $index=>$value){
						$vid = youtubeVideoId($value);
						echo '<meta property="og:image" content="http://i4.ytimg.com/vi/'.$vid.'/hqdefault.jpg" />';
					}

					foreach($arrimgs as $index=>$value){
							if($arrimgs[$index]!="") $foto = IMG_THUMB.$arrimgs[$index]; else $foto = HOST.'layout/img/logo-hip-fb.jpg';
								echo '<meta property="og:image" content="'.$foto.'" />';
					}


				}
				break;

				default:

					echo' 
					<meta property="og:site_name" content="..:: HiP! Comunicação | +55 34 3234-8207 ::.." />
					<meta property="og:title" content="Expressar Alegria, Entusiasmo, Prazer por uma Vitória." />
					<meta property="og:type" content="blog" />
					<meta property="og:url" content="'.HOST.'"/>
					<meta property="og:image" content="'.HOST.'layout/img/logo-hip-fb.jpg" />
					<meta property="fb:admins" content="759782736" />
					<meta name="description" content="Fundada em 2005, a HiP! é uma agência de publicidade, especializada nos métodos, na arte e na técnica publicitária, através de profissionais qualificados que estudam e analisam cada cliente com o objetivo de promover a venda de mercadorias, produtos, serviços e/ou imagem, prezando pelas boas práticas éticas e comerciais entre os principais agentes da publicidade" />';

				break;
			}

		}

}

?>
