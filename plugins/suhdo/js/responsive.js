//RESPONSIVE
$(window).load(function(){

	$('<div class="break"></div>').appendTo(".tiles");
	
	//set boxes as responsive element
	$('.size-10,.size-20,.size-25,.size-30,.size-33,.size-40,.size-50,.size-60,.size-67,.size-70,.size-80,.size-90,.size-100').addClass('responsive-box');
});


//RESIZE WINDOW
$(window).resize(function() {

 var windowx = $(window).width();
  if(windowx <= 480){
		$(".responsive-box").css({width:"100%"});
		$(".tiles-container").css({width:windowx});
  }

});

