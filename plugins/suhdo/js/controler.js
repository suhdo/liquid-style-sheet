//INTERFACE CONTROLER

PROTOCOL = 'http';
LOCATION = 'localhost';


var Write = {

	 Template : function(local,template){ return new this.Class(local,template) },

	 Class : function(local,template){

		
			var myurl = jsonUrl();

			if(myurl['module']==local)
				$.ajax({
				  url: "layout/php/writeTemplate.php?template="+template+"&local="+local+"&root="+myurl['root']+"&module="+myurl['module']+"&property="+myurl['property']+"&page="+myurl['page']+"&extra="+myurl['extra'],
				  context: document.body,
				  success: function(msg){ 
					$("[template="+template+"]").html(msg); 
				  }
				});

	}

}

var UI = {

	 Banner : function(jquerySelectorElement,bannerWidth,bannerHeight,rotateMS,fx){ return new this.Class(jquerySelectorElement,bannerWidth,bannerHeight,rotateMS,fx) },
	 Class : function(jquerySelectorElement,bannerWidth,bannerHeight,rotateMS,fx){

				$(jquerySelectorElement).fadeIn();
				var imgsLength = $(jquerySelectorElement+" .img").size();
				$(jquerySelectorElement).css({height:bannerHeight,width:bannerWidth,overflow:"hidden"});

//				$(jquerySelectorElement).fadeIn(250,'easeInCirc');
				$(jquerySelectorElement+" .img").css({height:bannerHeight,width:bannerWidth});
//				$(jquerySelectorElement+" .nav-left").css({height:bannerHeight,width:'49%'}).addClass("");
//				$(jquerySelectorElement+" .nav-right").css({height:bannerHeight,width:'49%'}).addClass("");
				$(jquerySelectorElement+" .img").css({height:bannerHeight,width:bannerWidth});


				if(imgsLength<=1) $(".nav-right,.nav-left").remove();

				$(jquerySelectorElement+" .img:nth(0)").css({"display":"block"}).addClass('selected');


				intervalo  = setInterval(function(){

					var indexImg = $(jquerySelectorElement+" .img.selected").index();

					if(indexImg<imgsLength-1){
						$(jquerySelectorElement+" .img").hide('slide',250).removeClass('selected');
						$(jquerySelectorElement+" .img:nth("+(indexImg+1)+")").effect('slide',250).addClass('selected');
					}else{
						$(jquerySelectorElement+" .img").hide('slide',250).removeClass('selected');
						$(jquerySelectorElement+" .img:nth(0)").effect('slide',250).addClass('selected');
					}

				},rotateMS);


				$('.nav-right').click(function(){

					if(intervalo) clearInterval(intervalo);
					var indexImg = $(jquerySelectorElement+" .img.selected").index();
					if(indexImg<imgsLength-1){
						$(jquerySelectorElement+" .img").hide('slide',250).removeClass('selected');
						$(jquerySelectorElement+" .img:nth("+(indexImg+1)+")").show('slide',250).addClass('selected');
						$('.nav-right').show();
						$('.nav-left').show();
					}else{
//						$(jquerySelectorElement+" .img").hide('slide',250).removeClass('selected');
//						$(jquerySelectorElement+" .img:nth(0)").show('slide',250).addClass('selected');
						$('.nav-right').hide();
						$('.nav-left').show();
					}
				});

				$('.nav-left').click(function(){

					if(intervalo) clearInterval(intervalo);
					var indexImg = $(jquerySelectorElement+" .img.selected").index();
					if(indexImg>=1){
						$(jquerySelectorElement+" .img").hide('slide',250).removeClass('selected');
						$(jquerySelectorElement+" .img:nth("+(indexImg-1)+")").show('slide',250).addClass('selected');
						$('.nav-right').show();
						$('.nav-left').show();
					}else{
						$('.nav-left').hide();
						$('.nav-right').show();

//						$(jquerySelectorElement+" .img").hide('slide',250).removeClass('selected');
//						$(jquerySelectorElement+" .img:nth("+(imgsLength-1)+")").show('slide','fast').addClass('selected');
					}
				});
	 }
}



/* Json Controler */
function myJson(jsonstring){

		return  eval("("+jsonstring+")");

}

function jsonUrl(){

   // retorna array url js
   var u = window.location.toString();
   var pureurl = u.split(PROTOCOL+"://");

   var pieces = pureurl[1].split("/");
   //alert(pieces.length);
   if(LOCATION=='www') var minus = 1; else var minus = 0;

   if((pieces[1-minus]==undefined)||(pieces[1-minus]=='')) var me = 'root'; else var me = pieces[1-minus];
   var strjson	= '{"root":"'+me+'","module":"'+pieces[2-minus]+'","property":"'+pieces[3-minus]+'","page":"'+pieces[4-minus]+'","extra":"'+pieces[5-minus]+'"}';
	
   return eval("("+strjson+")");

}

